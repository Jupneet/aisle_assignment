import interfaces.IGameOfLife;
import utilities.NeighbourGraph;

import java.util.*;

public class GameOfLife implements IGameOfLife {

    private NeighbourGraph graph;

    public GameOfLife(){
        graph = new NeighbourGraph();
    }

    @Override
    public boolean isAlone(int numberOfAliveNeighbours) {
        return numberOfAliveNeighbours < 2;
    }

    @Override
    public boolean isCrowded(int numberOfAliveNeighbours) {
        return numberOfAliveNeighbours > 3;
    }

    @Override
    public boolean isNeutral(int numberOfAliveNeighbours) {
        return (numberOfAliveNeighbours == 2 || numberOfAliveNeighbours == 3);
    }

    @Override
    public void checkForRebirths(String currentNode, int numberOfAliveNeighbours, SortedSet nextTickCoordinates) {
        if(numberOfAliveNeighbours < 2){
            return;
        }
        if(numberOfAliveNeighbours >= 2){

            String[] tokens = currentNode.split(",");
            int x = Integer.parseInt(tokens[0]);
            int y = Integer.parseInt(tokens[1]);

            //right
            if(!graph.hasNode((x + 1) + "," + y)){
                int aliveNeighbours = getAliveNeighbours((x + 1) + "," + y);
                if(!nextTickCoordinates.contains((x + 1) + "," + y) && aliveNeighbours == 3){
                    nextTickCoordinates.add((x + 1) + "," + y);
                }
            }

            //left
            if(!graph.hasNode((x - 1) + "," + y)){
                int aliveNeighbours = getAliveNeighbours((x - 1) + "," + y);
                if(!nextTickCoordinates.contains((x - 1) + "," + y) && aliveNeighbours == 3){
                    nextTickCoordinates.add((x - 1) + "," + y);
                }
            }

            //up
            if(!graph.hasNode(x + "," + (y+1))){
                int aliveNeighbours = getAliveNeighbours(x + "," + (y+1));
                if(!nextTickCoordinates.contains(x  + "," + (y+1)) && aliveNeighbours == 3){
                    nextTickCoordinates.add(x  + "," + (y+1));
                }
            }

            //down
            if(!graph.hasNode(x  + "," + (y-1))){
                int aliveNeighbours = getAliveNeighbours(x + "," + (y-1));
                if(!nextTickCoordinates.contains(x  + "," + (y-1)) && aliveNeighbours == 3){
                    nextTickCoordinates.add(x  + "," + (y-1));
                }
            }

            //upper right
            if(!graph.hasNode((x + 1) + "," + (y+1))){
                int aliveNeighbours = getAliveNeighbours((x + 1) + "," + (y+1));
                if(!nextTickCoordinates.contains((x + 1) + "," + (y+1)) && aliveNeighbours == 3){
                    nextTickCoordinates.add((x + 1) + "," + (y+1));
                }
            }

            //upper left
            if(!graph.hasNode((x - 1) + "," + (y+1))){
                int aliveNeighbours = getAliveNeighbours((x - 1) + "," + (y+1));
                if(!nextTickCoordinates.contains((x - 1) + "," + (y+1)) && aliveNeighbours == 3){
                    nextTickCoordinates.add((x - 1) + "," + (y+1));
                }
            }

            //bottom right
            if(!graph.hasNode((x + 1) + "," + (y-1))){
                int aliveNeighbours = getAliveNeighbours((x + 1) + "," + (y-1));
                if(!nextTickCoordinates.contains((x + 1) + "," + (y-1)) && aliveNeighbours == 3){
                    nextTickCoordinates.add((x + 1) + "," + (y-1));
                }
            }

            //bottom left
            if(!graph.hasNode((x - 1) + "," + (y-1))){
                int aliveNeighbours = getAliveNeighbours((x - 1) + "," + (y-1));
                if(!nextTickCoordinates.contains((x - 1) + "," + (y-1)) && aliveNeighbours == 3){
                    nextTickCoordinates.add((x - 1) + "," + (y-1));
                }
            }
        }
    }

    @Override
    public int getAliveNeighbours(String currentNode) {
        int aliveNeighbours = 0;
        String[] tokens = currentNode.split(",");
        int x = Integer.parseInt(tokens[0]);
        int y = Integer.parseInt(tokens[1]);

        //right
        if(graph.hasNode((x + 1) + "," + y)){
            aliveNeighbours++;
        }

        //left
        if(graph.hasNode((x - 1) + "," + y)){
            aliveNeighbours++;
        }

        //up
        if(graph.hasNode(x + "," + (y+1))){
            aliveNeighbours++;
        }

        //down
        if(graph.hasNode(x  + "," + (y-1))){
            aliveNeighbours++;
        }

        //upper right
        if(graph.hasNode((x + 1) + "," + (y+1))){
            aliveNeighbours++;
        }

        //upper left
        if(graph.hasNode((x - 1) + "," + (y+1))){
            aliveNeighbours++;
        }

        //bottom right
        if(graph.hasNode((x + 1) + "," + (y-1))){
            aliveNeighbours++;
        }

        //bottom left
        if(graph.hasNode((x - 1) + "," + (y-1))){
            aliveNeighbours++;
        }

        return aliveNeighbours;
    }


    @Override
    public void addAliveCell(int x, int y) {
        graph.addVertex(x, y);
    }

    @Override
    public String getNextTick() {
        Set<String> visited = new HashSet<String>();
        SortedSet nextTickCoordinates = new TreeSet();
        for (String key : graph.getAllNodes()) {
            if (!visited.contains(key)) {
                graphTraversal(key, visited, nextTickCoordinates);
            }
        }
        String output = "";
        Iterator<String> itr = nextTickCoordinates.iterator();
        while (itr.hasNext()) {
            output = output + itr.next() + "\n";
        }

        return output;
    }

    private void graphTraversal(String startNode,Set<String> visited, SortedSet nextTickCoordinates){
        Queue<String> queue = new LinkedList<>();

        queue.add(startNode);
        visited.add(startNode);

        while (!queue.isEmpty()) {
            String currentNode = queue.remove();
            int numberOfNeighbours = graph.getOutDegree(currentNode);
            if(isNeutral(numberOfNeighbours)){
                if(!nextTickCoordinates.contains(currentNode)){
                    nextTickCoordinates.add(currentNode);
                }
            }

            checkForRebirths(currentNode, numberOfNeighbours, nextTickCoordinates);

//            System.out.println(currentNode + " OUT " + numberOfNeighbours);

            for (String n : graph.getNeighboursOfNode(currentNode)) {
                if (!visited.contains(n)) {
                    queue.add(n);
                    visited.add(n);
                }
            }
        }
    }
}
