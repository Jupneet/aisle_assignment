package utilities;

import java.util.*;

public class NeighbourGraph extends Graph<Integer>{

    private Map<String, List<String>> aliveNeighbourMap = new HashMap<>();

    @Override
    public void addVertex(Integer x, Integer y) {

        String nodeKey = x + "," + y;
        if(!aliveNeighbourMap.containsKey(nodeKey)){
            aliveNeighbourMap.put(nodeKey, new LinkedList<String>());
        }

        //right
        if(aliveNeighbourMap.containsKey((x + 1) + "," + y)){
            aliveNeighbourMap.get((x + 1) + "," + y).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add((x + 1) + "," + y);
        }

        //left
        if(aliveNeighbourMap.containsKey((x - 1) + "," + y)){
            aliveNeighbourMap.get((x - 1) + "," + y).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add((x - 1) + "," + y);
        }

        //up
        if(aliveNeighbourMap.containsKey(x+ "," + (y+1))){
            aliveNeighbourMap.get(x + "," + (y+1)).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add(x  + "," + (y+1));
        }
        //down
        if(aliveNeighbourMap.containsKey(x  + "," + (y-1))){
            aliveNeighbourMap.get(x  + "," + (y-1)).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add(x  + "," + (y-1));
        }
        //upper-right
        if(aliveNeighbourMap.containsKey((x + 1) + "," + (y+1))){
            aliveNeighbourMap.get((x + 1) + "," + (y+1)).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add((x + 1) + "," + (y+1));
        }
        //upper left
        if(aliveNeighbourMap.containsKey((x - 1) + "," + (y+1))){
            aliveNeighbourMap.get((x - 1) + "," + (y+1)).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add((x - 1) + "," + (y+1));
        }
        //bottom right
        if(aliveNeighbourMap.containsKey((x + 1) + "," + (y-1))){
            aliveNeighbourMap.get((x + 1) + "," + (y-1)).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add((x + 1) + "," + (y-1));
        }
        //bottom left
        if(aliveNeighbourMap.containsKey((x - 1) + "," + (y-1))){
            aliveNeighbourMap.get((x - 1) + "," + (y-1)).add(nodeKey);
            aliveNeighbourMap.get(nodeKey).add((x - 1) + "," + (y-1));
        }

    }

    public Set<String> getAllNodes() {
        return aliveNeighbourMap.keySet();
    }

    public int getOutDegree(String currentNode) {
        return aliveNeighbourMap.get(currentNode).size();
    }

    public List<String> getNeighboursOfNode(String node){
        return aliveNeighbourMap.get(node);
    }

    public boolean hasNode(String node){
        return aliveNeighbourMap.containsKey(node);
    }

}
