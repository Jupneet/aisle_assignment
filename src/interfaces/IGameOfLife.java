package interfaces;

import java.util.SortedSet;

public interface IGameOfLife {

    boolean isAlone(int numberOfAliveNeighbours);
    boolean isCrowded(int numberOfAliveNeighbours);
    boolean isNeutral(int numberOfAliveNeighbours);
    void checkForRebirths(String currentNode, int numberOfAliveNeighbours, SortedSet nextTickCoordinates);

    int getAliveNeighbours(String currentNode);

    void addAliveCell(int x, int y);

    String getNextTick();

}
