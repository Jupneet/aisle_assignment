import utilities.Graph;

import java.util.Scanner;

public class Aisle {
    public static void main(String[] args){
        System.out.println("\nPlease enter input for Game of life (all coordinates in one go, format : <x,y> and separate " +
                " them with a line ) and press enter..");

        GameOfLife game = new GameOfLife();

        Scanner userInput = new Scanner(System.in);
        String line;
        try {
            while ((line = userInput.nextLine()) != null) {
                if (line.isEmpty()) {
                    break;
                }
                String[] tokens = line.split(",");
                int x = Integer.parseInt(tokens[0]);
                int y = Integer.parseInt(tokens[1]);
                game.addAliveCell(x,y);
            }

            userInput.close();

            String nextTick = game.getNextTick();
            System.out.println("\n NEXT TICK \n");
            System.out.println(nextTick);

        }catch (NumberFormatException e){
            System.out.println("Please check the x and y coordinates. They should be an integer");
        }catch (Exception e){
            System.out.println("Exception occurred " + e);
        }
        finally {
            userInput.close();
        }
    }
}
